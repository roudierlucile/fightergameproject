package fr.lr.StreetFighter.Character;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import fr.lr.StreetFighter.Animation.AnimationOpt;

public class Goku extends Character implements Runnable {
	
	/*FOR ANIMATION */
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {8,4,6,3,4,4,4,6,4};
	private AnimationOpt animation;

	
	public Goku() {
		super();
		System.out.println("Create Goku");
	}
	//TEST CLASS FOR CHARACTER
	public Goku(int x,int y,int player) {
		super("Goku",x, y, player);
		this.idCharacter = 1;
		animateSprite();
	
	}
	

	public void animateSprite() {
		try 
		{
			
			//We load the image sprites
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("/fr/lr/StreetFighter/img/gokuFull.png"));
			sprites = new ArrayList<BufferedImage[]>();
			
			//We put all moves in for
			for(int i = 0; i < 9; i++)
			{
				
				//Put number of numFrames
				BufferedImage [] bi = new BufferedImage[numFrames[i]];
				for(int j = 0; j < numFrames[i]; j++)
				{
					//if stand
					if(i == STAND) {
						super.setHeight(165);
						super.setWidth(109);
						
						bi[j] = spritesheet.getSubimage(j *super.getWidth(), i * super.getHeight(), super.getWidth(),super.getHeight());						
				
					}
					
					//if angry
					else if(i == ANGRY) {
						super.setHeight(163);
						super.setWidth(68);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()+4, super.getWidth(),super.getHeight());
						
					}
					
					else if(i == WALKING) {
						super.setHeight(130);
						super.setWidth(110);
					
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()+70, super.getWidth(),super.getHeight());
						
					}
					
					
					else if(i==PROTECT) {
						super.setHeight(137);
						super.setWidth(99);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()+50, super.getWidth(),super.getHeight());
					}
					
					
					else if(i==JUMPING) {
						super.setHeight(169);
						super.setWidth(108);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()-80, super.getWidth(),super.getHeight());
					}
					
					
					else if(i==FALLING) {
						super.setHeight(169);
						super.setWidth(108);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()-80, super.getWidth(),super.getHeight());
					}
					
					else if(i==PUNCH) {
						super.setHeight(131);
						super.setWidth(147);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()+150, super.getWidth(),super.getHeight());
					}
					
					
					else if(i==KICK) {
						super.setHeight(159);
						super.setWidth(163);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()-45, super.getWidth(),super.getHeight());
					}
					
					else if(i==HIT) {
						super.setHeight(129);
						super.setWidth(98);
						bi[j] = spritesheet.getSubimage(j*super.getWidth(), i*super.getHeight()+200, super.getWidth(),super.getHeight());
					}
					/*
					else if(i==DIE) {
						height=150;
						width = 150;
						bi[j] = spritesheet.getSubimage(j*(height), i*(width+4), height,width);
					}
					*/
					
			
					
				}
				sprites.add(bi);
			}			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		
		animation = new AnimationOpt();
		current =STAND;
		animation.setFrames(sprites.get(current));
		animation.setDelay(100);
		
		
		}
		
	public void update() {
		move();
		
		//IF HE WALKING
			if(left || right) {
				this.setY(200);
				if(current != WALKING) {
				
					current = WALKING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(40);
			
				}
			}
			
			//IF JUMPING
		else if(jump) {
				if(current != JUMPING) {
					
					current = JUMPING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(-1);
		
				}
		}
			
			//IF FALLING
		else if(falling) {
							if(current != FALLING) {
					current = FALLING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(150);
				
			}
		}
			
			//PROTECT
		else if(protect) {
			if(current != PROTECT) {
				current = PROTECT;
				animation.setFrames(sprites.get(current));
				animation.setDelay(-1);
			}
		}
			
		else if(punch) {
			if(current != PUNCH) {
				current = PUNCH;
				animation.setFrames(sprites.get(current));
				animation.setDelay(70);
			}
		}
			
	//if kick
		else if(kick) {
			if(current != KICK) {
				current = KICK;
				animation.setFrames(sprites.get(current));
				animation.setDelay(70);
			}
		}
		
			//IF HIT
			
		else if(hit) {
			if(current != HIT) {
				current = HIT;
				animation.setFrames(sprites.get(current));
				animation.setDelay(100);
			}
		}
	//IF DO NOTHING
			else {
				if(current != ANGRY) {
					current = ANGRY;
					animation.setFrames(sprites.get(current));
					animation.setDelay(150);
				}
			}
			
		
			
			//PLAY ONCE THE FIGHT ACTION
			if(current == PUNCH) {
				if(animation.hasPlayedOnce()) {
					punch = false;
				}
			}
			if(current == KICK) {
				if(animation.hasPlayedOnce()) {
					kick = false;
				}
			}
			
			if(current == HIT) {
				if(animation.hasPlayedOnce()) {
					hit = false;
				}
			}
		
			animation.update();
	}
	
	public void draw (Graphics2D g2d) {
		super.draw(g2d);
		update();
		animation.update();
		
		g2d.drawImage(animation.getImage(),(int)this.getX(),(int)this.getY(), null);
	}
}
