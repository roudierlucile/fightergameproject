package fr.lr.StreetFighter.Character;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Health {
	double width;
	double max;
	public int x,y;

	public Health(double healthM, int x) {
		super();
		this.width = healthM;
		
		this.x = x;
		//this.y = y;
		this.max = healthM;
	}
	



	public double getWidth() {
		return width;
	}

	public void setWidth(double loseHealth) {
		this.width = loseHealth;
	}

	public double getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void draw(Graphics2D g) {
		
		
		
		g.setColor(new Color(219, 30, 30));
		g.fill3DRect(x, 20, (int) max, 20,true);
		
		
		g.setColor(new Color(108, 249, 52));
		g.fill3DRect(x, 20, (int)width, 20,true);
		
		g.setColor(new Color(57, 165, 29));
		g.drawRect(x, 20, (int) max, 20);
		
	}
	
	
	
}
