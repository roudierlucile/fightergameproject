package fr.lr.StreetFighter.Character;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import fr.lr.StreetFighter.son.Sound;


abstract class Character implements Runnable {
	
	protected int idCharacter;
	//Taille et position
	private double x;
	private double y;
	protected int width;
	protected int height;
	
	//HEALTH
	protected int maxH;
	protected double healthM;
	protected Health health;
	
	//ACTION
	protected boolean walk = true;
	protected boolean left;
	protected boolean right;
	protected boolean jump;
	protected boolean punch;
	protected boolean kick;
	protected boolean protect;
	protected boolean dead = false;
	protected boolean hit;
	protected boolean falling = true;
	protected double gravity = 1.5;
	
	//Combo
	protected float combo = 0;
	protected int nbKick = 0;
	protected int nbPunch = 0;
	
	protected int scoreTotal = 0;
	
	
	//current action
	protected static final int STAND = 0;
	protected static final int ANGRY = 1;
	protected static final int WALKING=2;
	protected static final int PROTECT=3;
	protected static final int JUMPING =4;
	protected static final int FALLING = 5;
	protected static final int PUNCH =6;
	protected static final int KICK = 7;
	protected static final int HIT = 8;
	protected static final int DIE = 9;
	protected int current;
	
	
	//thread
	public Thread jumping;
	//PLAYER
	protected int player;
	
	//Name
	protected String name;
	
	protected BufferedImage sprite;
	
	protected int maxY;
	protected double speed = 0.6;
	protected double speedFall = 0.8;
	//private double maxSpeed = 1.6;
	protected boolean inLive;

	
	/*SCORE*/
	protected int score;
	
	public boolean running = true;
	protected Sound soundPunch;
	private Sound soundKick;
	
	
	public Character() {
		System.out.println("Create a character");
	}
	/**
	 * We init the character with sound and position
	 * we try to know if is the first or second
	 * @param name
	 * @param x
	 * @param y
	 * @param player
	 */
	
	public Character(String name,int x,int y,int player) {
		//super();
		
		
		//SOUND
		this.soundPunch = new Sound(getClass().getResource("/fr/lr/StreetFighter/music/punch.wav"));
		this.soundKick = new Sound(getClass().getResource("/fr/lr/StreetFighter/music/kick.wav"));
	
		
		this.name=name;
		this.player = player;
		healthM = 250;
		maxH = 250;
		
		if(player == 1) {health = new Health(getHealthM(),20);}
		if(player == 2) {health = new Health(getHealthM(),700+20 - maxH);}
		
		
		this.x = x;
		this.y = y;
		this.maxY= y;
		this.inLive = true;

		
		jumping = new Thread(this);
		jumping.start();
	}

	/*
	 * All assec for the character
	 * 
	 */
	
	
	//CURRENT
	public int getCurrent() {return current;}
	public int getIdCharacter() {
		return idCharacter;
	}
	public void setIdCharacter(int idCharacter) {
		this.idCharacter = idCharacter;
	}
	public void setCurrent(int current) {this.current = current;}
	
	//Name
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	//ACTION
	public boolean isLeft() {return left;}
	public void setLeft(boolean left) {this.left = left;}
	public boolean isRight() {return right;}
	public void setRight(boolean right) {this.right = right;}
	public boolean isJump() {return jump;}
	public void setJump(boolean jump) {	this.jump = jump;}
	public boolean isPunch() {return punch;}
	public void setPunch(boolean punch) {this.punch = punch;}
	public boolean isKick() {return kick;}
	public void setKick(boolean kick) {this.kick = kick;}
	public boolean isProtect() {return protect;}
	public void setProtect(boolean protect) {this.protect = protect;}
	public boolean isFalling() {return falling;}
	public void setFalling(boolean falling) {this.falling = falling;}
	public boolean isInLive() {return inLive;}
	public void setInLive(boolean inLive) {this.inLive = inLive;}
	public boolean isDead() {return dead;}
	public void setDead(boolean dead) {this.dead = dead;}


	//Position
	public double getX() {return x;}
	public void setX(int x) {this.x = x;}
	public double getY() {return y;}
	public void setY(int y) {this.y = y;}
	public int getMaxY() {return maxY;}
	public void setMaxY(int maxY) {this.maxY = maxY;}

	//player
	public int getPlayer() {return player;}
	public void setPlayer(int player) {this.player = player;}

	//Width/height
	public int getWidth() {return width;}
	public void setWidth(int width) {this.width = width;}
	public int getHeight() {return height;}
	public void setHeight(int height) {this.height = height;}
	
		//HEALTH
	public double getHealthM() {return healthM;}
	public void setHealthM(double d) {this.healthM = d;}
	public Health getHealth() {return health;}
	public void setHealth(Health health) {this.health = health;}

	
		//SCORE
	public int getScore() {return score;}
	public void setScore(int score) {this.score = score;}

	
	
	
	public int getScoreTotal() {return scoreTotal;	}

	public void setScoreTotal(int scoreTotal) {this.scoreTotal = scoreTotal;}

	public void update() {
		
		while(!dead) {
			move();
		}
	}
	
	/**
	 * Function to move
	 * left and right is for direction
	 * walk is to see if the character move
	 */
	public void move() {
	
		/* Si c'est gauche
		 * 		Si x - la vitesse superieur � 0, x prend - la vitesse
		 *		Sinon va de l'autre cot�
		 *Inverse pour droite
		 */
		
		
		if(left && walk) {
			
			walk = true;
			if(x > 0) {x -= speed;}
			else {x = 750 - width ;}
			
		}if(right && walk) {
			walk = true;
			if(x < 750) {x+=speed;}
			else {x =0;}
		}
		fall();
		jump();

	}
	
	
	/**
	 * Fall is when a character jump we need to know the gravity to have the speed of falling
	 * Is just physics
	 *
	 */
	//FALLING
	public void fall() {
		if(falling && !jump) {
			y += gravity;
			if(y > getMaxY()) y = getMaxY();
			if(y == getMaxY()) falling = false;
		}
	}
	
	/**
	 * When the char jump
	 */
	public void jump() {
		if(jump) {
			y -= speedFall;	
			if(y < 25 ) {jump=false;}
			y -= speedFall;
			falling = true;
		}
	}
	
	
			/**
			 * We check if the character touche another char
			 * @param c
			 */
	public void checkAttack(Character c) {
		//Collision punch if right or left
		if(right || left) {
			if(this.getX() <= c.getX() + c.getWidth()
			&& this.getX() + this.getWidth() >= c.getX()) {
					walk=false;
			}else {
				walk=true;
			}
		}else {walk=true;}
			
			//If the attack touch
			attackTouch(c);
			
			//Edit the score
			checkScore(c);
	}
	
	/**
	 * if the character attack another
	 * we see if the char touch another with an attack
	 * if attack and touch, char lose life else no
	 * @param c
	 */
	public void attackTouch(Character c) {
		if(this.getX() <= c.getX() + c.getWidth()
		&& this.getX() + this.getWidth() >= c.getX()) {
			if(punch && !c.protect && !c.dead) {
				this.soundPunch.startSound();
				double loseHealth=c.health.getWidth()- 0.08;
				c.health.setWidth(loseHealth);
				c.hit = true;
				//for combo
				combo += 1;
				scoreTotal += combo;
				
			}
			if(kick && !c.protect && !c.dead) {
				this.soundKick.startSound();
				double loseHealth=c.health.getWidth()- 0.5;
				c.health.setWidth(loseHealth);
				c.hit = true;
				//for combo
				
				combo +=1;
				scoreTotal += combo;
			}
			//If you touch and you attack you win point
			checkPointScore();
		}
		checkCombo(c);	
	}
	
	
	/**
	 *  We check if the ennemy punch or kick
	 *  if he do that our combo return0
	 * @param c
	 */
	
	public void checkCombo(Character c){
		if(c.kick || c.punch) {
			this.combo = 0;
		}
	}
	
	
	/**
	 * Add point if you punch or you kick
	 */
	
	public void checkPointScore() {
		if(this.kick) {
			this.nbKick += 2;
		}
		if(this.punch) {
			this.nbPunch += 1;
		}
	}
	/**
	 * We check the score to the caracter with another character
	 * the max of the score is 3
	 * we see with the health
	 * 
	 * @param c
	 */
	// condition for the score
	public void checkScore(Character c) {
		if(score == 3 ||c.score == 3) {
			scoreTotal += nbKick + nbPunch;
			dead = true;
			running = false;
		}
		
		
		//if the health is 0 and the score not 3 we can add a +1 to the score
			if(c.health.getWidth() <= 0 && c.score != 3) {
				this.score += 1;
				c.health.setWidth(c.healthM);
				this.health.setWidth(this.healthM);
		
			}
			if(this.health.getWidth()<=0 && score !=3) {
				c.score += 1;
				c.health.setWidth(c.healthM);
				this.health.setWidth(this.healthM);
		
			}
	}
	
	
	/**
	 * Draw the character and the score
	 * @param g
	 */
	//For draw everything
	public void draw(Graphics2D g) {
		g.setColor(Color.WHITE);
		Font font = new Font("Arial", Font.BOLD,30);
		g.setFont(font);
			if(player == 1) {
				
				g.drawString(score + " -", 340, 40);
			
				g.setColor(new Color(219, 30, 30));
				g.drawString("Combo: " + this.combo, 50, 70);
				
			}
			if(player == 2) {
				g.drawString("- "+score, 370, 40);
				g.setColor(new Color(219, 30, 30));
				g.drawString("Combo: " + this.combo, 500, 70);
				
			}
		health.draw(g);
	}
	
	
	/**
	 * We init the score
	 */
	//init score
	public void initScore() {
		this.score = 0;
	}
	
	/**
	 * Thread to move the character
	 */
	//THREAD TO MOVE
	public void run() {
		while(running) {
			while(!dead) {
				move();				
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}


