package fr.lr.StreetFighter.Character;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
//import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import fr.lr.StreetFighter.Animation.AnimationOpt;

public class Broly extends Character {
	
	
	/*FOR ANIMATION */
	private ArrayList<BufferedImage[]> sprites;
	private final int[] numFrames = {9,8,12,3,3,5,5,8,4,3};
	private AnimationOpt animation;
	
	//TEST CLASS FOR CHARACTER
	
	public Broly(int x,int y,int player) {
		
		super("Broly",x, y, player);
		this.idCharacter = 2;
		animateSprite();
		//update();
	
	}
	
	public void animateSprite() {
		try 
		{
			
			//We load the image sprites
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("/fr/lr/StreetFighter/img/brolyFinal.gif"));
			sprites = new ArrayList<BufferedImage[]>();
			
			//We put all moves in for
			for(int i = 0; i < 10; i++)
			{
				
				//Put number of numFrames
				BufferedImage [] bi = new BufferedImage[numFrames[i]];
				for(int j = 0; j < numFrames[i]; j++)
				{
					//if stand
					if(i == STAND) {
						
						height=121;
						width=199;
						
						bi[j] = spritesheet.getSubimage(j *(height+2), i * width, height-2,width);						
				
					}
					
					//if angry
					else if(i == ANGRY) {
						height=122;
						width = 198;
					
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width+4), height-2,width);
						
					}
					
					else if(i == WALKING) {
						height=128;
						width = 203;
					
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width), height,width);
						
					}
					else if(i==PROTECT) {
						height=128;
						width = 201;
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width + 3), height,width);
					}
					
					else if(i==JUMPING) {
						height=156;
						width = 208;
					
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width-4), height,width);
					}
					else if(i==FALLING) {
						height=156;
						width = 208;
					
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width-2), height,width-1);
					}
					
					else if(i==PUNCH) {
						height=177;
						width = 191;
						
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width+16), height,width-1);
					}
					
					else if(i==KICK) {
						height=201;
						width = 203;
						bi[j] = spritesheet.getSubimage(j*(height+3), i*(width+2), height,width+1);
					}
					
					else if(i==HIT) {
						height=140;
						width = 196;
						bi[j] = spritesheet.getSubimage(j*(height+2), i*(width+10), height,width-5);
					}
					
					else if(i==DIE) {
						height=150;
						width = 150;
						bi[j] = spritesheet.getSubimage(j*(height), i*(width+4), height,width);
					}
					
					
			
					
				}
				sprites.add(bi);
			}			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		
		animation = new AnimationOpt();
		current = STAND;
		animation.setFrames(sprites.get(current));
		animation.setDelay(100);
		
		
		}
		
	public void update() {
		move();
		
		//IF HE WALKING
		//if(current == PUNCH ) { if(animation.hasPlayedOnce())punch=false; }
		//if(current == KICK ) { if(animation.hasPlayedOnce())kick=false; }
			if(left || right) {
				if(current != WALKING) {
				
					current = WALKING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(40);
			
				}
			}
			
			//IF JUMPING
		else if(jump) {
				if(current != JUMPING) {
				
					current = JUMPING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(-1);
		
				}
		}
			
			//IF FALLING
		else if(falling) {
							if(current != FALLING) {
					current = FALLING;
					animation.setFrames(sprites.get(current));
					animation.setDelay(100);
				
			}
		}
			
			//PROTECT
		else if(protect) {
			if(current != PROTECT) {
				current = PROTECT;
				animation.setFrames(sprites.get(current));
				animation.setDelay(-1);
			}
		}
			
		else if(punch) {
			if(current != PUNCH) {
				current = PUNCH;
				animation.setFrames(sprites.get(current));
				animation.setDelay(70);
			}
		}
			
	//if kick
		else if(kick) {
			if(current != KICK) {
				current = KICK;
				animation.setFrames(sprites.get(current));
				animation.setDelay(70);
			}
		}
	//IF HIT
			
		else if(hit) {
			if(current != HIT) {
				current = HIT;
				animation.setFrames(sprites.get(current));
				animation.setDelay(100);
			}
		}
			
	//IF DO NOTHING
			else {
				if(current != STAND) {
					current = STAND;
					animation.setFrames(sprites.get(current));
					animation.setDelay(100);
				}
			}
			
			
			//PLAY ONCE THE FIGHT ACTION
			if(current == PUNCH) {
				if(animation.hasPlayedOnce()) {
					punch = false;
				}
			}
			if(current == KICK) {
				if(animation.hasPlayedOnce()) {
					kick = false;
				}
			}
			
			if(current == HIT) {
				if(animation.hasPlayedOnce()) {
					hit = false;
				}
			}

		animation.update();
	}
	
	public void draw (Graphics2D g2d) {
		super.draw(g2d);
		update();
		animation.update();
		g2d.drawImage(animation.getImage(),(int)this.getX(),(int)this.getY(), null);
	}
}