package fr.lr.StreetFighter;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.sql.SQLException;

import javax.swing.JPanel;

import fr.lr.StreetFighter.Character.Broly;
import fr.lr.StreetFighter.Character.Goku;
import fr.lr.StreetFighter.Database.PlayerDAO;
import fr.lr.StreetFighter.clavier.Clavier;
import fr.lr.StreetFighter.map.Area;
import fr.lr.StreetFighter.player.PlayerWin;
import fr.lr.StreetFighter.son.Sound;
import fr.lr.StreetFighter.time.Chrono;

@SuppressWarnings("serial")
public class Game extends JPanel implements Runnable{
	private PlayerDAO playerDao;
	
	private Sound soundDie;		//Sound when you die
	private Sound soundBkg;		//Sound of background

	private Area area;					//Import the area of game
	
	public Goku goku;			//Player Goku	
	public Broly broly;			//Player Brolt
	
	public int CharWin;
	public int ScoreWin;
	
	public boolean close = false; //close the window
	
	public boolean replay = false;			//Replay the game
	public boolean end = false;				//the end	
	public boolean quit = false;			//to quit the gamme

	
	
	/**
	 * We init the game with sound et component like characters and area
	 */
	public Game() {
	
		
		//Sound
		this.soundBkg = new Sound(getClass().getResource("/fr/lr/StreetFighter/music/DBZfight.wav"));
		soundBkg.startSound();
		this.soundDie = new Sound(getClass().getResource("/fr/lr/StreetFighter/music/die.wav"));
		
		area = new Area();
		
		this.goku = new Goku(50,200,1);
		this.broly = new Broly(600,150,2);
		
		//sthis.area = new Area((Character) goku,(Character) broly);
		this.setFocusable(true);
		this.requestFocusInWindow();
		//move
		this.addKeyListener(new Clavier());
		
		Thread thread = new Thread(new Chrono());
		thread.start();

	}
	
	/**
	 * We init the game
	 * the game return to zero
	 */
public void initGame() {
		//INIT THE MOVE
		soundDie.closeSound();
		goku.setDead(false);
		broly.setDead(false);
		
		end = false;
		replay = false;
		
		goku.checkAttack(broly) ;
		broly.checkAttack(goku);
		soundBkg.startSound();
	}
	
	
	
	/**
	 * 
	 * Paint component of the game
	 * when we have the score
	 * when we have the end of the game
	 */
	public void paintComponent(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		g.drawImage(this.area.getImgBkg(),0,0,null);
		Font font = new Font("Arial", Font.BOLD,30);
		goku.draw(g2);
		broly.draw(g2); 
	
		
		if(!end) {
			initGame();
		}

		//Check the end game
		if(goku.getScore() == 3 || broly.getScore() == 3) {
			soundDie.startSound();
			end = true;
			soundBkg.closeSound();
			if(goku.getScore() == 3) {
				ScoreWin = goku.getScoreTotal();
				CharWin = goku.getIdCharacter();
				g.setColor(Color.GREEN);
				g.setFont(font);
				g.drawString(goku.getName() + " WIN",250, 150);
				g.drawString("Score total: " + goku.getScoreTotal(), 200, 150);
			
			}else if(broly.getScore() == 3) {
				ScoreWin = broly.getScoreTotal();
				CharWin = broly.getIdCharacter();
				g.setColor(Color.GREEN);
				g.setFont(font);
				g.drawString(broly.getName() + " WIN",250, 150);
				g.drawString("Score total: " + broly.getScoreTotal(), 200, 120);
			}
			
			goku.setDead(true);
			broly.setDead(true);
			
			g.setFont(font);
			g.setColor(Color.BLACK);
			g.drawString("PRESS [Y] TO REPLAY [N] IF YOU DON'T", 100, 200);
			
			//REPLAY THE GAME
			if(replay) {
				replay();
			}
			//QUIT THE GAME
			if(quit) {
				close = true;
				
				this.setVisible(false);
				new PlayerWin(CharWin,ScoreWin);
				
			
				}
		}
			
	}

	
	/**
	 * Func for replay the game
	 */
		public void replay() {
			goku.setX(50); goku.setY(200);
			broly.setX(600); broly.setY(150);
			goku.setScore(0);
			broly.setScore(0);
			initGame();
		}
	@Override
	public void run() {
		while(true) {
		
			if(end) {
				broly.running = false;
				goku.running = false;
			}
		}
		
	}
	
}
