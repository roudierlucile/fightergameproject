package fr.lr.StreetFighter.Intro;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import fr.lr.StreetFighter.son.Sound;



public class Intro extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static boolean activeBtn = false;
	//background
	public Sound sound;
	public ImageIcon icoBkg;
	public Image imgBkg;
	public IntroWin introwin;
	public JButton btn;
	//public boolean activeBtn = false;
	
	Font font = new Font("Arial",Font.PLAIN,17);
	
	public Intro() {
		//Background
		initComponent();
				
	}
	
	public void initComponent() {
		this.setLayout(null);
		this.icoBkg = new ImageIcon(getClass().getResource("/fr/lr/StreetFighter/img/bcg3.png"));
		this.imgBkg = this.icoBkg.getImage();
		this.sound = new Sound(getClass().getResource("/fr/lr/StreetFighter/music/DBZ.wav"));
		sound.startSound();
		
		btn = new JButton(new ImageIcon(getClass().getResource("/fr/lr/StreetFighter/img/startPlay2.png")));
		btn.setBounds(275, 250,200, 79);
		btn.setBackground(Color.WHITE);
		//btn.setHorizontalTextPosition(SwingConstants.LEFT);
		btn.setBorder(null); 
	}
	public void paintComponent(Graphics g) {
		g.drawImage(this.imgBkg,0,0,null);
	}

	
 
}
