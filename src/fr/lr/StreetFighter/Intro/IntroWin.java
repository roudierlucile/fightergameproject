package fr.lr.StreetFighter.Intro;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


import fr.lr.StreetFighter.GameWin;



public class IntroWin extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//public static Intro intro;
	public Intro intro;
	public JButton btn;
	public static boolean play =false;
	public IntroWin() {
		initComponents();
	}
		public void initComponents() {
			// TODO Auto-generated method stub
			
			JFrame fenetre = new JFrame("Street Ball");
			fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fenetre.setSize(750, 400);
			fenetre.setLocationRelativeTo(null);
			fenetre.setResizable(false);
			fenetre.setAlwaysOnTop(true);
			
			
			//Instance de l'obj scene
			intro = new Intro();
		

			intro.btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					fenetre.setVisible(false);
					intro.sound.closeSound();
					dispose();
					new GameWin();
					
				}
				
			});
			
		
			intro.add(intro.btn);
			fenetre.setContentPane(intro);
			fenetre.setVisible(true);
		}

}
