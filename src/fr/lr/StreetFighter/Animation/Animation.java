package fr.lr.StreetFighter.Animation;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Animation {
	ArrayList<BufferedImage> frames;
	public BufferedImage sprite;
	private boolean run = false;
	private long previousTime,speed;
	private int frameAtPause,currentFrame;
	private boolean playedOnce;
	
	public Animation(ArrayList<BufferedImage> frames) {
		this.frames = frames;
		playedOnce = false;
		
	}
	
	public void update(long time) {
		if(run) {
			if(time - previousTime >= speed) {
				currentFrame++;
				try {
				sprite = frames.get(currentFrame);
				playedOnce = false;
				}catch(IndexOutOfBoundsException e) {
					
					currentFrame = 0;
					sprite = frames.get(currentFrame);
					playedOnce = true;
				}
				previousTime = time;
				
			}
			
		}
	}
	
	public void start() {
		run = true;
		previousTime = 0;
		frameAtPause = 0;
		currentFrame = 0;
	}
	
	public void stop(){
		run =false;
		previousTime = 0;
		frameAtPause = 0;
		currentFrame = 0;
	}
	
	public void pause() {
	
		frameAtPause = currentFrame;
		run = false;
	}
	
	public void resume() {
		currentFrame = frameAtPause;
		run = true;
	}

	public long getSpeed() {
		return speed;
	}

	public void setSpeed(long speed) {
		this.speed = speed;
	}

	public boolean isPlayedOnce() {
		return playedOnce;
	}

	public void setPlayedOnce(boolean playedOnce) {
		this.playedOnce = playedOnce;
	}

	public boolean isRun() {
		return run;
	}

	public void setRun(boolean run) {
		this.run = run;
	}
	
	
	
}
