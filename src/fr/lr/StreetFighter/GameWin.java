package fr.lr.StreetFighter;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class GameWin extends JFrame {
	public static Game game;
	
	public GameWin() {
		initComponents();
	}
		public void initComponents() {
			// TODO Auto-generated method stub
			
			JFrame fenetre = new JFrame("Street Fight");
			fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			fenetre.setSize(750, 400);
			fenetre.setLocationRelativeTo(null);
			fenetre.setResizable(false);
			fenetre.setAlwaysOnTop(true);
			
			
			//Instance de l'obj scene
			game = new Game();
			
			fenetre.setContentPane(game); //asso de la fen a l'application 
			fenetre.setVisible(true);
			
			if(game.close) {
				this.dispose();
			}
		}
	
}
