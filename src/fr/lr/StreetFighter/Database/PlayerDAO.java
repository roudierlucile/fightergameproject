package fr.lr.StreetFighter.Database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;



public class PlayerDAO {
	private Connect database;
	
	public PlayerDAO() {
		database = new Connect();
	}
	
	
	
	/**
	 * Show all information about the player in database
	 */
	public ArrayList<Player> showPlayer() {
		 //Cr�ation d'un objet Statement
			ArrayList<Player> players = new ArrayList<Player>();
	      Statement state = null;
		try {
			state = database.conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      //L'objet ResultSet contient le r�sultat de la requ�te SQL
		String sql="SELECT * FROM player JOIN charactergame ON charactergame.idCharacter = player.idCharacter ORDER BY score DESC LIMIT 10;";
		ResultSet rs;
		try {
			rs = state.executeQuery(sql);
			Player player;
			 
			while(rs.next()) {
				player = new Player(rs.getString("name"),rs.getInt("idPlayer"),rs.getInt("score"),rs.getString("nom"));
				players.add(player);
			}
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 try {
			database.conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      return players;
	}
	
	
	/**
	 * Insert the player in database
	 * @param name
	 * @param score
	 * @throws SQLException 
	 */
	public void insertPlayer(String name,int character, int score) throws SQLException {
		   String sql = "INSERT INTO player (name,score, dateRegister,idCharacter) VALUES (?,?,NOW(),?);";
		   PreparedStatement pst = database.conn.prepareStatement(sql);
		   pst.setString(1, name);
		   pst.setInt(2,score);
		   pst.setInt(3,character);
		   	   
		   pst.execute();
		   database.conn.close();
		   pst.close();
	}
}
