package fr.lr.StreetFighter.Database;

public class Player {

	private String name;
	private int idP;
	private int score;
	private String perso;
	
	public Player(String name, int idP, int score, String perso) {
		super();
		this.name = name;
		this.idP = idP;
		this.score = score;
		this.perso = perso;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdP() {
		return idP;
	}
	public void setIdP(int idP) {
		this.idP = idP;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getPerso() {
		return perso;
	}
	public void setPerso(String perso) {
		this.perso = perso;
	}
	
	
}
