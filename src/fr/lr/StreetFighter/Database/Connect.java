package fr.lr.StreetFighter.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
	String url = "jdbc:mysql://localhost:3306/fighter?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=true";
	String user ="root";
	String pass ="root";
	Connection conn;
	
	public Connect() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(ClassNotFoundException e) {e.printStackTrace();}
		
		try {
			conn = DriverManager.getConnection(url,user,pass);
			System.out.println("Connection successful");
		} catch (SQLException e) {e.printStackTrace();}
	
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	
	
}

