package fr.lr.StreetFighter.clavier;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import fr.lr.StreetFighter.GameWin;


public class Clavier implements KeyListener {

	@Override
	
	public void keyPressed(KeyEvent e) {
		
		if(GameWin.game.end) {
			if(e.getKeyCode() == KeyEvent.VK_Y) {
				GameWin.game.goku.setDead(false);
				GameWin.game.broly.setDead(false);
				System.out.println("test");
				GameWin.game.replay = true;
			}
			if(e.getKeyCode() == KeyEvent.VK_N) {
				GameWin.game.quit = true;
			}
		}
	
		//PLAYER ONE
	if((!GameWin.game.goku.isDead()) || (!GameWin.game.broly.isDead())) {
		if(e.getKeyCode() == KeyEvent.VK_LEFT) { 
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setLeft(true);
			}
			if(GameWin.game.broly.getPlayer() == 1) {
				GameWin.game.broly.setLeft(true);
			}
			

		
		}else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setRight(true);
				
			}
			if(GameWin.game.broly.getPlayer() == 1) {
				GameWin.game.broly.setRight(true);
			}
		
		}else if (e.getKeyCode() == KeyEvent.VK_UP) {
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setJump(true);
		}
			if(GameWin.game.broly.getPlayer() == 1) {
				GameWin.game.broly.setJump(true);
		}

		}else if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setPunch(true);
			}
			if(GameWin.game.broly.getPlayer() == 1) {
				GameWin.game.broly.setPunch(true);
			}
			
			
			
		}else if(e.getKeyCode() == KeyEvent.VK_ASTERISK) {
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setKick(true);
			
		}if(GameWin.game.broly.getPlayer() == 1) {
			GameWin.game.broly.setKick(true);
			
	}
			
		
		}
		
		else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if(GameWin.game.goku.getPlayer() == 1) {
				GameWin.game.goku.setProtect(true);
			
		}if(GameWin.game.broly.getPlayer() == 1) {
			GameWin.game.broly.setProtect(true);
			
	}
			
		
		}
		
		
		//PLAYER TWO
		
		if(e.getKeyCode() == KeyEvent.VK_Q) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setLeft(true);
			}
			if(GameWin.game.broly.getPlayer() == 2) {
				GameWin.game.broly.setLeft(true);
			}
			

		
		}else if (e.getKeyCode() == KeyEvent.VK_D) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setRight(true);
				
			}
			if(GameWin.game.broly.getPlayer() == 2) {
				GameWin.game.broly.setRight(true);
				
			}
		
		}else if (e.getKeyCode() == KeyEvent.VK_Z) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setJump(true);
		}
			if(GameWin.game.broly.getPlayer() == 2) {
				GameWin.game.broly.setJump(true);
				
		}

		}else if(e.getKeyCode() == KeyEvent.VK_W) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setPunch(true);
			}
			if(GameWin.game.broly.getPlayer() == 2) {
				GameWin.game.broly.setPunch(true);
			}
			
			
			
		}else if(e.getKeyCode() == KeyEvent.VK_X) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setKick(true);
			
		}if(GameWin.game.broly.getPlayer() == 2) {
			GameWin.game.broly.setKick(true);
			
	}
			
		
		}
		
		else if(e.getKeyCode() == KeyEvent.VK_S) {
			if(GameWin.game.goku.getPlayer() == 2) {
				GameWin.game.goku.setProtect(true);
			
		}if(GameWin.game.broly.getPlayer() == 2) {
			GameWin.game.broly.setProtect(true);
			
	}
			
		
		}
		
	}else{
		gameFalse();
	}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
		gameFalse();
		
	}

	public void gameFalse() {
		//KARA
				GameWin.game.goku.setRight(false);
				GameWin.game.goku.setLeft(false);
				GameWin.game.goku.setJump(false);
				GameWin.game.goku.setProtect(false);
				
				//KYU
				GameWin.game.broly.setRight(false);
				GameWin.game.broly.setLeft(false);
				GameWin.game.broly.setJump(false);
				GameWin.game.broly.setProtect(false);
				
				GameWin.game.replay = false;
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
