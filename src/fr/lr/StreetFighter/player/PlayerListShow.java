package fr.lr.StreetFighter.player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.lr.StreetFighter.Database.Player;
import fr.lr.StreetFighter.Database.PlayerDAO;

public class PlayerListShow extends JPanel{
	
	//info
	
	private int character;
	private int score;
	
	//Write
	private JTextField tPseudo;
	
	//Label for info
	private JLabel title;
	private JLabel listPlayer;
	private JLabel namePlayer;
	private JLabel scorePlayer;
	private JLabel lPseudo;
	private JLabel lscore;
	
	//font
	Font font = new Font("Arial",Font.PLAIN,17);
	//Import player list
	private PlayerDAO playerDAO;

public PlayerListShow(int character, int score){
	playerDAO = new PlayerDAO();
	
	this.character = character;
	this.score = score;
	initComponent();
	

}

public void initComponent() {
	this.setBackground(Color.BLACK);
	this.setLayout(null);

	title = new JLabel("LIST OF PLAYERS");
	title.setForeground(Color.WHITE);
	title.setFont(font);
	title.setBounds(300, 5,200, 50);
	this.add(title);
	
	listPlayer = new JLabel("_______ BEST PLAYERS _______");
	listPlayer.setForeground(Color.WHITE);
	listPlayer.setFont(font);
	listPlayer.setBounds(400, 60,400, 50);
	this.add(listPlayer);
	
	lPseudo = new JLabel("Who you are ?");
	lPseudo.setForeground(Color.WHITE);
	lPseudo.setFont(font);
	lPseudo.setBounds(50, 100,200, 50);
	this.add(lPseudo);
	
	scorePlayer = new JLabel("Your score: " + this.score);
	scorePlayer.setForeground(Color.WHITE);
	scorePlayer.setFont(font);
	scorePlayer.setBounds(50, 220,200, 50);
	this.add(scorePlayer);
	
	//textfield
	tPseudo = new JTextField("Enter your name");
	tPseudo.setBackground(Color.white);
	tPseudo.setBounds(30, 150, 200, 30);
	//tPseudo.setPreferredSize(new Dimension(150,30));
	tPseudo.setBorder(null);
	this.add(tPseudo);
	
	JButton btn = new JButton("Enter");
	btn.setBounds(30, 190, 200, 30);
	btn.setBackground(Color.WHITE);
	this.add(btn);
	
	btn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String pseudo = tPseudo.getText();			
			System.exit(0);
			try {
				playerDAO.insertPlayer(pseudo, character, score);
			
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	});
	
	showAllPlayers();
	
}


public void showAllPlayers() {
	
	int x = 90;
	ArrayList<Player> players = playerDAO.showPlayer();
	for(Player player: players) {
		System.out.println(player.getName());
		
		namePlayer = new JLabel("Player: " + player.getName());
		namePlayer.setForeground(Color.WHITE);
		namePlayer.setFont(font);
		namePlayer.setBounds(400, x,200, 50);
		this.add(namePlayer);
		
		lscore = new JLabel("Score: " + player.getScore());
		lscore.setForeground(Color.WHITE);
		lscore.setFont(font);
		lscore.setBounds(550, x,200, 50);
		this.add(lscore);
		x+=20;
		//listPlayer.setText(player.getName());
		
	}
	
	
	
	
}

}