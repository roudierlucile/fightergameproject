package fr.lr.StreetFighter.player;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.lr.StreetFighter.Game;
import fr.lr.StreetFighter.Database.Player;
import fr.lr.StreetFighter.Database.PlayerDAO;

public class PlayerWin extends JFrame{
	public static PlayerListShow playerShow;
	
	private int character;
	private int score;
	
	public PlayerWin(int character, int score) {
		super();

		
		this.character = character;
		this.score = score;
		initComponents();
	}
	
	public void initComponents() {
		// TODO Auto-generated method stub
		
		JFrame fenetre = new JFrame("Player - StreetFight");
		fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetre.setSize(750, 400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setResizable(false);
		
		
		
		//Instance de l'obj scene
		playerShow = new PlayerListShow(character,score);
		
		fenetre.setContentPane(playerShow); //asso de la fen a l'application 
		
		fenetre.setVisible(true);
	}
	
	
	
	
}
