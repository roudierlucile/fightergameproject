package fr.lr.StreetFighter.son;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound {
	private Clip clip;
	 private AudioFormat format;
	 
	 
	 /**
	  * 
	  * 
	  * @param url take the path of the sound
	  * search the audio input stream to have de path et open with clip
	  */
	public Sound(URL url) {
		
		try {
			AudioInputStream aid = AudioSystem.getAudioInputStream(new File(url.toURI()));
			clip = AudioSystem.getClip();
			clip.open(aid);
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	/*
	 * Start the music
	 */
	
	public void startSound() {
		clip.start();
	}
	
	/*
	 * Stop the music
	 */
	public void closeSound() {
		clip.close();
	}
}
