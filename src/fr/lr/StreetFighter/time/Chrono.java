package fr.lr.StreetFighter.time;

import fr.lr.StreetFighter.GameWin;

public class Chrono implements Runnable{

	private final int PAUSE = 3;

	public void run() {
		
		while(true) {

			GameWin.game.repaint();
			try {
				Thread.sleep(PAUSE);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
